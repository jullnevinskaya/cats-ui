import type { Page, TestFixture } from '@playwright/test';
import { test } from '@playwright/test';
import { unsuccessResponse } from '../__mocks__/unsuccess-response';
/**
 * Класс для реализации логики страницы с рейтингами.
 *
 */
export class RaitingPage {
  private page: Page;
  public raitingSelector: string

  constructor({page}: {page: Page;}) {
    this.page = page;
    this.raitingSelector = '//td[@class="rating-names_item-count__1LGDH has-text-success"]'
  }

  async openRaitingPage() {
    return await test.step('Открываю страницу "Рейтинг имён котиков"', async () => {
      await this.page.goto('/rating')
    })
  }

  async getRaiting() {
    const elements = await this.page
      .locator(this.raitingSelector)
      .allTextContents();
    return elements.map(el => parseInt(el));
  }
}

export type RaitingPageFixture = TestFixture<RaitingPage, {page: Page;}>;

export const raitingPageFixture: RaitingPageFixture = async ({ page }, use) => {
  const raitingPage = new RaitingPage({ page });
  await use(raitingPage);
};