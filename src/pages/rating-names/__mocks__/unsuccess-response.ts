export const unsuccessResponse = {
  status: 503,
  contentType: 'application/json',
  body: JSON.stringify({
    error: 'Service Temporarily Unavailable',
    message: 'The server is currently unable to handle the request due to a temporary overload or scheduled maintenance. Please try again later.',
  }),
};
