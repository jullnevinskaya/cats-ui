import { RaitingPage, raitingPageFixture} from '../__page-object__'
import { test as base, expect } from 'playwright/test';
import { unsuccessResponse } from '../__mocks__/unsuccess-response';

const test = base.extend<{ raitingPage: RaitingPage }>({
  raitingPage: raitingPageFixture,
});

test('При ошибке сервера в методе rating - отображается попап ошибки', async ({
    page,
    raitingPage, }) => {
  await page.route(
    request => request.href.includes('/cats/rating'),
    async route => {
      await route.fulfill(unsuccessResponse);
    });
  await raitingPage.openRaitingPage();
  await expect(page.getByText('Ошибка загрузки рейтинга')).toBeVisible();
});

test('Рейтинг котиков отображается правильно', async ({ page, raitingPage }) => {
  await raitingPage.openRaitingPage();
  await page.waitForSelector(raitingPage.raitingSelector);
  const arr = await raitingPage.getRaiting();
  // console.log(arr);
  let isRight = true;
  for (let i = 1; i < arr.length; i++) {
    if (arr[i - 1] < arr[i]) {
      isRight = false;
      break;
    }
  }
  expect(isRight).toBeTruthy();
});
